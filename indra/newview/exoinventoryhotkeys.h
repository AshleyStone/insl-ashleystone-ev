/** 
 * @file exoinventoryhotkeys.h
 * @brief EXOInventoryHotkeys class definition
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXO_INVENTORY_HOTKEYS
#define EXO_INVENTORY_HOTKEYS

#include "llfloater.h"

class LLInventoryItem;
class LLViewerInventoryItem;

class exoBindedInventory
{
public:
	exoBindedInventory();
	exoBindedInventory(LLUUID id, std::string label, KEY key, MASK mask, bool replace);

public:
	KEY mKey;
	MASK mMask;
	LLUUID mItem;
	std::string mLabel;
	bool mReplace;
};

class exoInventoryHotkeys : public LLFloater
{
	friend class LLFloaterReg;
	
private:
	exoInventoryHotkeys(const LLSD& key);
	~exoInventoryHotkeys();
	
	static exoInventoryHotkeys* sInstance;

public:
	static exoInventoryHotkeys* getInstance();

private:
	void draw();

	BOOL postBuild();
	BOOL initBindings();
	
public:
	std::string getLabel(LLUUID id);

	void bindInventory(LLInventoryItem* item);
	void bindInventory(LLInventoryItem* item, KEY key, MASK mask);
	void bindInventory(LLInventoryItem* item, std::string label, KEY key, MASK mask);

	BOOL toggleInventory(LLViewerInventoryItem* item, std::string label, bool replace);
	
	BOOL triggerBinding(KEY key, MASK mask);

private:
	void systemTip(std::string data);

	typedef std::map<LLUUID, exoBindedInventory*> item_map_t;
	item_map_t mBindings;
};

#endif // EXO_INVENTORY_HOTKEYS
