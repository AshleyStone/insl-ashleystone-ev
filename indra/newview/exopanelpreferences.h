/** 
 * @file exopanelpreferences.cpp
 * @brief Side tray Exodus settings panel
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXO_PANEL_PREFERENCES
#define EXO_PANEL_PREFERENCES

#include "llfloater.h"

class LLCheckBoxCtrl;

class exoFloaterPreferences : public LLFloater
{
public:
	exoFloaterPreferences(const LLSD& key);
	~exoFloaterPreferences(){};

	BOOL postBuild();
	
	void onOpen(const LLSD& key);
	bool notifyChildren(const LLSD& info);

	void setKey(KEY key, bool type);

private:
	void onClickSetKey(const LLSD& user_data);
	void onClickPreviewSound(const LLSD& user_data);
	void onClickScriptTheme(const LLSD& user_data);
	void onClickChangeSkin(const LLSD& user_data);

	LLCheckBoxCtrl* mScriptThresholdCheck;
	S32 scriptThreshold;

	void toggleScriptThreshold();
};

#endif // EXO_PANEL_PREFERENCES
