/**
 * @file exonotifiermacosx.cpp
 * @brief Displays notifications via Notification Center
 *
 * $LicenseInfo:firstyear=2012&license=viewerlgpl$
 * Copyright (C) 2012 Katharine Berry
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"
#include "llviewerwindow.h"
#include "llwindow.h"
#include "exonotifiermacosx.h"

#include <notifications/notifications.h>
#include <CoreServices/CoreServices.h>

exoNotifierMacOSX::exoNotifierMacOSX() {
	notifications::prepare();
	notifications::set_activation_callback(&exoNotifierMacOSX::notificationActivated);
}

// Ignore the deprecation of Gestalt until Apple provides a replacement
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
bool exoNotifierMacOSX::isUsable() {
	// Only usable on OS X 10.8+
	SInt32 major_version, minor_version;
	OSErr r1 = Gestalt(gestaltSystemVersionMajor, &major_version);
	OSErr r2 = Gestalt(gestaltSystemVersionMinor, &minor_version);
	if(r1 != noErr || r2 != noErr)
	{
		LL_WARNS("exoNotifier") << "Unable to determine OS X version." << LL_ENDL;
		return false;
	}
	return ((major_version >= 10 && minor_version >= 8) || major_version >= 11);
}
// Restore deprecation errors (because 'warning' would override -Werror)
#pragma GCC diagnostic error "-Wdeprecated-declarations"

void exoNotifierMacOSX::showNotification(const std::string& notification_title, const std::string& notification_message, const std::string& notification_type)
{
	LL_INFOS("exoNotifier") << "Showing notification center notification." << LL_ENDL;
	notifications::notify(notification_title, "", notification_message);
}

void exoNotifierMacOSX::clearDelivered()
{
	notifications::clear_all();
}

// static
void exoNotifierMacOSX::notificationActivated()
{
	gViewerWindow->getWindow()->bringToFront();
}
