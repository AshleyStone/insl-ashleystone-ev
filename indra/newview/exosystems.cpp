/** 
 * @file exosystems.cpp
 * @brief System used for downloading client detection database and other bits.
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "exosystems.h"

#include "llagentcamera.h"
#include "llbufferstream.h"
#include "llfloaterreg.h"
#include "llhttpclient.h"
#include "llkeyboard.h"
#include "llsdserialize.h"
#include "llviewercontrol.h"
#include "llversioninfo.h"
#include "llviewermedia.h"
#include "lleventtimer.h"
#include "llparcel.h"
#include "llviewerparcelmgr.h"
#include "llagent.h"

#include "exofloaterteleportrequest.h"

std::string exoSystems::loginMessage;

//std::string exoSystems::updateTitle;
//std::string exoSystems::updateMessage;
//std::string exoSystems::updateURL;

std::string exoSystems::databaseBackendLocation;
std::string exoSystems::databaseBackendLabel;
std::string exoSystems::databaseBackendSalt;

//bool exoSystems::isBlacklisted = false;

bool exoSystems::hasLoginMessage = false;

KEY exoSystems::mRTAKey;
KEY exoSystems::mRTDKey;

bool exoSystems::disableSpacebar = false;

class exoSystemsClient : public LLHTTPClient::Responder
{
public:
	exoSystemsClient(){}

	virtual void completedRaw(U32 status, const std::string& reason, const LLChannelDescriptors& channels, const LLIOPipe::buffer_ptr_t& buffer)
	{
		if (isGoodStatus(status))
		{
			LLBufferStream llbuffer(channels, buffer.get());
			std::stringstream body;
			body << llbuffer.rdbuf();

			LLSD data;
			std::istringstream istr(body.str());
			LLSDSerialize::fromXML(data, istr);

			if (data.isDefined() && data.has("valid"))
			{
				exoSystems::phraseData(data);

				const std::string filename = gDirUtilp->getExpandedFilename(LL_PATH_USER_SETTINGS, "exodus.xml");

				llofstream xml_file(filename);
				if (xml_file.is_open())
				{
					LLSDSerialize::toPrettyXML(data, xml_file);
					xml_file.close();
				}
			}
			else exoSystems::useLocalData();
		}
		else exoSystems::useLocalData();
	}
};

exoSystems::exoSystems()
	: LLEventTimer(5.f)
{
}

void exoSystems::getData()
{
	if (gSavedSettings.getBOOL("ExodusUpdateData"))
	{
		LLSD headers;
		headers.insert("User-Agent", LLViewerMedia::getCurrentUserAgent());
		headers.insert("Agent-Version", LLVersionInfo::getVersion());

		LLHTTPClient::get("http://app.exodusviewer.com/data/", new exoSystemsClient(), headers);
	}
	else useLocalData();

	updateSettings();
}

void exoSystems::useLocalData()
{
	bool phrasedData = false;

	LLSD data;
	std::string filename = gDirUtilp->getExpandedFilename(LL_PATH_USER_SETTINGS, "exodus.xml");
	llifstream xml_file(filename);
	if (xml_file.is_open() && LLSDSerialize::fromXML(data, xml_file) >= 1)
	{
		if (data.has("valid"))
		{
			phraseData(data);
			phrasedData = true;
		}

		xml_file.close();
	}

	if (!phrasedData) useReleaseData();
}

void exoSystems::useReleaseData()
{
	std::string client_list_filename = gDirUtilp->getExpandedFilename(LL_PATH_APP_SETTINGS, "exodus.xml");
	
	LLSD data;
	llifstream xml_file(client_list_filename);

	if (xml_file.is_open() && LLSDSerialize::fromXML(data, xml_file) >= 1)
	{
		if (data.has("valid")) phraseData(data);

		xml_file.close();
	}
}

void exoSystems::phraseData(LLSD data)
{
	if (data.has("motd"))
	{
		loginMessage = data["motd"].asString();
		hasLoginMessage = true;
	}

	// Maybe later.
	/*if (data.has("latest"))
	{
		LLSD update = data["latest"];
		if (update.has("version") && update["version"] != LLVersionInfo::getVersion())
		{
			updateTitle = update["message"].asString();
			updateMessage = update["message"].asString();
			updateURL = update["url"].asString();
			hasUpdate = true;
		}
	}*/

	// Maybe never.
	/*if (data.has("blacklist"))
	{
		if (data["blacklist"].has(LLVersionInfo::getAgentVersion()))
		{
			isBlacklisted = true;

			std::string message = data["blacklist"][LLVersionInfo::getAgentVersion()].asString();
			if (message.length())
			{
				// Display recieved message.
			}
			else
			{
				// Display default warning message.
			}
		}
	}*/
}

void exoSystems::updateSettings()
{
	std::string keyString = gSavedSettings.getString("ExodusTeleportRequestAcceptButton");

	if (LLKeyboard::keyFromString(keyString, &mRTAKey))
	{
		LLStringUtil::toUpper(keyString); // Avoid manual edits of the debug setting?
		disableSpacebar = keyString == "SPACE";
	}
	else mRTAKey = KEY_NONE;

	keyString = gSavedSettings.getString("ExodusTeleportRequestDeclineButton");

	if (LLKeyboard::keyFromString(keyString, &mRTDKey))
	{
		LLStringUtil::toUpper(keyString); // Avoid manual edits of the debug setting?
		if (!disableSpacebar) disableSpacebar = keyString == "SPACE";
	}
	else mRTDKey = KEY_NONE;
}

BOOL exoSystems::keyDown(KEY key, MASK mask, BOOL repeated)
{
	return (key == mRTAKey || key == mRTDKey) && gAgentCamera.cameraMouselook();
}

BOOL exoSystems::keyUp(KEY key, MASK mask)
{
	if (mRTAKey == KEY_NONE || mRTDKey == KEY_NONE) return FALSE;

	if (gAgentCamera.cameraMouselook() || gSavedSettings.getBOOL("ExodusTeleportRequestMouselook"))
	{
		if (key == mRTAKey)
		{
			exoFloaterTeleportRequest* req = dynamic_cast<exoFloaterTeleportRequest*>(LLFloaterReg::getLastFloaterInGroup("exo_request_teleport"));
			if (req)
			{
				req->requestReply();

				if (gSavedSettings.getBOOL("ExodusTeleportRequestPlayAcceptSoundAlert"))
				{
					make_ui_sound("ExodusTeleportRequestAcceptSoundAlert");
				}
			}

			return gAgentCamera.cameraMouselook();
		}
		else if (key == mRTDKey)
		{
			exoFloaterTeleportRequest* req = dynamic_cast<exoFloaterTeleportRequest*>(LLFloaterReg::getLastFloaterInGroup("exo_request_teleport"));
			if (req)
			{
				req->requestReply("decline");

				if (gSavedSettings.getBOOL("ExodusTeleportRequestPlayDeclineSoundAlert"))
				{
					make_ui_sound("ExodusTeleportRequestDeclineSoundAlert");
				}
			}

			return gAgentCamera.cameraMouselook();
		}
	}

	return FALSE;
}

void exoSystems::handleTeleport(U32 type)
{
	bool workload = false;

	LLFloaterReg::const_instance_list_t& inst_list = LLFloaterReg::getFloaterList("exo_request_teleport");
	for (LLFloaterReg::const_instance_list_t::const_iterator iter = inst_list.begin(); iter != inst_list.end(); /*iter++*/)
	{

		exoFloaterTeleportRequest* req = dynamic_cast<exoFloaterTeleportRequest*>(*iter++);
		if (req)
		{
			req->requestReply("teleport");
			workload = true;
		}
	}

	if (workload && gSavedSettings.getBOOL("ExodusTeleportRequestPlayDeclineSoundAlertOnDeath"))
	{
		make_ui_sound("ExodusTeleportRequestDeclineSoundAlert");
	}
}

BOOL exoSystems::tick()
{
	if (drawDistance != 0.f)
	{
		gAgentCamera.mDrawDistance = drawDistance;
	}

	mEventTimer.stop();

	return TRUE;
}

void exoSystems::smashDrawDistance()
{
	drawDistance = gAgentCamera.mDrawDistance;
	gAgentCamera.mDrawDistance = 0.f;

	mEventTimer.start();
}

BOOL exoSystems::getStarted()
{
	return mEventTimer.getStarted();
}

LLUUID exoSystems::getRezGroup()
{
	LLParcel *parcel = LLViewerParcelMgr::getInstance()->getAgentParcel();
	if (gSavedSettings.getBOOL("ExodusChooseBestRezGroup"))
	{
		if (gAgent.isInGroup(parcel->getGroupID()))
		{
			return parcel->getGroupID();
		}
		else if (gAgent.isInGroup(parcel->getOwnerID()))
		{
			return parcel->getOwnerID();
		}
	}
	return gAgent.getGroupID();
}
