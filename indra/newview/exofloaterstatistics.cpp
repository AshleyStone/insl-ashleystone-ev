/** 
 * @file exofloaterstatistics.cpp
 * @brief The alternative lightweight statistics window.
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "message.h"

#include "llfloaterreg.h"
#include "exofloaterstatistics.h"

#include "llagent.h"
#include "llappviewer.h"
#include "llbutton.h"
#include "lltextbox.h"
#include "llviewercontrol.h"
#include "llviewerstats.h"
#include "llviewerregion.h"

// exodus_floater_statistics.xml

exoFloaterStatistics::exoFloaterStatistics(const LLSD& key) :
	LLFloater(key)
{
}

// Todo: Resize draggable area to number of lines of text?

BOOL exoFloaterStatistics::postBuild()
{
	mText = getChild<LLTextBox>("statistics_text");
	mText->setDoubleClickCallback(boost::bind(&exoFloaterStatistics::onDoubleClick, this, _1, _2, _3, _4));
	mText->setHAlign((LLFontGL::HAlign)gSavedSettings.getS32("ExodusMinimalStatisticsHAlign"));

	getChild<LLButton>("align_left")->setCommitCallback(boost::bind(&exoFloaterStatistics::textAlignLeft, this));
	getChild<LLButton>("align_center")->setCommitCallback(boost::bind(&exoFloaterStatistics::textAlignCenter, this));
	getChild<LLButton>("align_right")->setCommitCallback(boost::bind(&exoFloaterStatistics::textAlignRight, this));
	
	windowSize = getRect();
	renderBackground = gSavedSettings.getBOOL("ExodusMinimalStatisticsBackground");
	if (!renderBackground)
	{
		LLRect textSize = mText->calcScreenRect();
		textSize.setCenterAndSize(textSize.getCenterX() - 10, textSize.getCenterY(), textSize.getWidth(), textSize.getHeight() + 20);

		setRect(textSize);

		setCanResize(FALSE);
		setCanClose(FALSE);
	}

	return TRUE;
}

void exoFloaterStatistics::draw()
{
	F32 time = gFrameTimeSeconds;

	S32 mins = (S32)(time / 60);
	S32 secs = (S32)(time - (mins * 60));

	std::string text = llformat(
		"%d:%02d | %dfps, %1.1fkbps, %dms | %1.1fsf, %1.1ftd",
		mins, secs,
		(S32)LLViewerStats::getInstance()->mFPSStat.getMeanPerSec(),
		LLViewerStats::getInstance()->mKBitStat.getMeanPerSec(),
		(S32)LLViewerStats::getInstance()->mSimPingStat.getPrev(0),
		LLViewerStats::getInstance()->mSimFPS.getPrev(0),
		LLViewerStats::getInstance()->mSimTimeDilation.getPrev(0)
	);

	mText->setText(text);

	if (renderBackground)
	{
		LLFloater::draw();
	}
	else if (mText->getVisible() && mText->getRect().isValid()) // Render just the text...
	{
		LLView* rootp = LLUI::getRootView();
		LLRect screen_rect = mText->calcScreenRect();

		if (rootp->getLocalRect().overlaps(screen_rect) && LLUI::sDirtyRect.overlaps(screen_rect))
		{
			LLUI::pushMatrix();
			LLUI::translate((F32)mText->getRect().mLeft, (F32)mText->getRect().mBottom, 0.f);

			mText->draw();

			LLUI::popMatrix();
		}
	}
}

BOOL exoFloaterStatistics::onDoubleClick(LLUICtrl* ctrl, S32 x, S32 y, MASK mask)
{
	renderBackground = !renderBackground;

	gSavedSettings.setBOOL("ExodusMinimalStatisticsBackground", renderBackground);

	setCanResize(renderBackground);
	setCanClose(renderBackground);
	
	if (renderBackground)
	{
		LLRect windowPosition = getRect();
		windowSize.setCenterAndSize(windowPosition.getCenterX() + 10, windowPosition.getCenterY() + 13, windowSize.getWidth(), windowSize.getHeight());

		setRect(windowSize);
	}
	else
	{
		windowSize = getRect();

		LLRect textSize = mText->calcScreenRect();
		textSize.setCenterAndSize(textSize.getCenterX() - 10, textSize.getCenterY(), textSize.getWidth(), textSize.getHeight() + 20);

		setRect(textSize); 
	}

	return TRUE;
}

void exoFloaterStatistics::textAlignLeft()
{
	gSavedSettings.setS32("ExodusMinimalStatisticsHAlign", (S32)LLFontGL::LEFT);

	mText->setHAlign(LLFontGL::LEFT);
}

void exoFloaterStatistics::textAlignCenter()
{
	gSavedSettings.setS32("ExodusMinimalStatisticsHAlign", (S32)LLFontGL::HCENTER);

	mText->setHAlign(LLFontGL::HCENTER);
}

void exoFloaterStatistics::textAlignRight()
{
	gSavedSettings.setS32("ExodusMinimalStatisticsHAlign", (S32)LLFontGL::RIGHT);

	mText->setHAlign(LLFontGL::RIGHT);
}
