/** 
 * @file llnetmap.cpp
 * @author James Cook, Ayamo Nozaki
 * @brief Display of surrounding regions, objects, and agents. 
 * @note This file is heavily modified for Exodus Viewer!
 *
 * $LicenseInfo:firstyear=2001&license=viewerlgpl$
 * Second Life Viewer Source Code
 * Copyright (C) 2001-2010, Linden Research, Inc.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Linden Research, Inc., 945 Battery Street, San Francisco, CA  94111  USA
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "llnetmap.h"

// Library includes (should move below)
#include "indra_constants.h"
#include "llavatarnamecache.h"
#include "llmath.h"
#include "llfloaterreg.h"
#include "llfocusmgr.h"
#include "lllocalcliprect.h"
#include "llrender.h"
#include "llui.h"
#include "lltooltip.h"

#include "llglheaders.h"

// Viewer includes
#include "llagent.h"
#include "llagentcamera.h"
#include "llagentui.h" // <exodus/>
#include "llappviewer.h" // for gDisconnected
#include "llcallingcard.h" // LLAvatarTracker
#include "llfloaterworldmap.h"
#include "lltracker.h"
#include "llsurface.h"
#include "llviewercamera.h"
#include "llviewercontrol.h"
#include "llviewertexture.h"
#include "llviewertexturelist.h"
#include "llviewermenu.h"
#include "llviewerobjectlist.h"
#include "llviewerregion.h"
#include "llviewerwindow.h"
#include "llworld.h"
#include "llworldmapview.h"		// shared draw code

// <exodus>
#include "llavataractions.h"
#include "llmutelist.h"
#include "llnotifications.h"
#include "llnotificationmanager.h"
#include "llnotificationsutil.h"
#include "llviewermenu.h"
#include "llvoavatar.h"
#include "exosystems.h"
// </exodus>

static LLDefaultChildRegistry::Register<LLNetMap> r1("net_map");

const F32 LLNetMap::MAP_SCALE_MIN = 32; // <exodus/>
const F32 LLNetMap::MAP_SCALE_MID = 512; // <exodus/>
const F32 LLNetMap::MAP_SCALE_MAX = 4096;

const F32 MAP_SCALE_INCREMENT = 16;
const F32 MAP_SCALE_ZOOM_FACTOR = 1.10f; // <exodus/> Zoom in factor per click of scroll wheel (10%)
const F32 MIN_DOT_RADIUS = 3.5f;
const F32 DOT_SCALE = 0.75f;
const F32 MIN_PICK_SCALE = 2.f;
const S32 MOUSE_DRAG_SLOP = 2;		// How far the mouse needs to move before we think it's a drag

std::map<LLUUID, LLVector3d>	LLNetMap::mClosestAgentsToCursor; // <exodus/>
std::map<LLUUID, LLVector3d>	LLNetMap::mClosestAgentsAtLastClick; // <exodus/>
std::map<LLUUID, LLColor4>		LLNetMap::avatarColors; // <exodus/>

LLNetMap::LLNetMap (const Params & p)
:	LLUICtrl (p),
	mBackgroundColor (p.bg_color()),
	mScale(gSavedSettings.getF32("ExodusMinimapScale")), // <exodus/>
	mPickRadius(gSavedSettings.getF32("ExodusMinimapAreaEffect")), // <exodus/>
	mPixelsPerMeter(MAP_SCALE_MID / REGION_WIDTH_METERS),
	mObjectMapTPM(0.f),
	mObjectMapPixels(0.f),
	mTargetPan(0.f, 0.f),
	mCurPan(0.f, 0.f),
	mStartPan(0.f, 0.f),
	mMouseDown(0, 0),
	mPanning(false),
	mUpdateNow(false),
	mObjectImageCenterGlobal(), // <exodus/>
	mObjectRawImagep(),
	mObjectImagep(),
	mClosestAgentToCursor(),
	menuMinimap(NULL) // <exodus/>
{
	mDotRadius = llmax(DOT_SCALE * mPixelsPerMeter, MIN_DOT_RADIUS);
}

LLNetMap::~LLNetMap()
{
}

BOOL LLNetMap::postBuild()
{
	// <exodus>
	LLUICtrl::CommitCallbackRegistry::ScopedRegistrar registrar;
	registrar.add("Minimap.Zoom", boost::bind(&LLNetMap::handleZoom, this, _2));
	registrar.add("Minimap.Tracker", boost::bind(&LLNetMap::handleStopTracking, this, _2));

	menuMinimap = LLUICtrlFactory::getInstance()->createFromFile<LLMenuGL>("exo_menu_mini_map_default.xml", gMenuHolder, LLViewerMenuHolderGL::child_registry_t::instance());
	// </exodus>

	return TRUE;
}

void LLNetMap::setScale( F32 scale )
{
	scale = llclamp(scale, MAP_SCALE_MIN, MAP_SCALE_MAX);

	gSavedSettings.setF32("ExodusMinimapScale", scale); // <exodus/>

	mCurPan *= scale / mScale;
	mScale = scale;

	if (mObjectImagep.notNull())
	{
		F32 width = (F32)(getRect().getWidth());
		F32 height = (F32)(getRect().getHeight());
		F32 diameter = sqrt(width * width + height * height);
		F32 region_widths = diameter / mScale;
		F32 meters = region_widths * LLWorld::getInstance()->getRegionWidthInMeters();
		F32 num_pixels = (F32)mObjectImagep->getWidth();
		mObjectMapTPM = num_pixels / meters;
		mObjectMapPixels = diameter;
	}

	mPixelsPerMeter = mScale / REGION_WIDTH_METERS;
	mDotRadius = llmax(DOT_SCALE * mPixelsPerMeter, MIN_DOT_RADIUS);

	mUpdateNow = true;
}

// <exodus> Ayamo was here! :3
void LLNetMap::draw()
{
	LLViewerRegion* region = gAgent.getRegion();

	if (region == NULL) return;

 	static LLFrameTimer map_timer;
	static LLUIColor map_track_color = LLUIColorTable::instance().getColor("MapTrackColor", LLColor4::white);
	//static LLUIColor map_track_disabled_color = LLUIColorTable::instance().getColor("MapTrackDisabledColor", LLColor4::white);
	static LLUIColor map_frustum_color = LLUIColorTable::instance().getColor("MapFrustumColor", LLColor4::white);
	static LLCachedControl<LLColor4> map_avatar_color(gSavedSettings, "ExodusMapAvatarColor", LLColor4::green);
	static LLCachedControl<LLColor4> map_avatar_friend_color(gSavedSettings, "ExodusMapAvatarFriendColor", LLColor4::yellow);
	static LLCachedControl<LLColor4> map_linden_color(gSavedSettings, "ExodusMapLindenColor", LLColor4::blue);
	static LLCachedControl<LLColor4> map_avatar_rollover_color(gSavedSettings, "ExodusMapRolloverColor", LLColor4::cyan);
	static LLCachedControl<LLColor4> map_laser_color(gSavedSettings, "ExodusMapLaserColor", LLColor4::red);
	static LLCachedControl<bool> map_projectial_trajectory(gSavedSettings, "ExodusMinimapProjectialTrajectory", false);
	static LLCachedControl<LLColor4> map_projectial_line_color(gSavedSettings, "ExodusMinimapProjectialLaserColor", LLColor4::yellow);
	static LLCachedControl<F32> map_projectial_buoyancy(gSavedSettings, "ExodusMinimapProjectialBuoyancy", 0.8f);
	static LLCachedControl<F32> map_projectial_velocity(gSavedSettings, "ExodusMinimapProjectialVelocity", 25.f);
	static LLCachedControl<bool> center_to_region(gSavedSettings, "ExodusMinimapCenter", false);
	static LLCachedControl<bool> auto_center(gSavedSettings, "MiniMapAutoCenter", true);
	static LLCachedControl<bool> rotate_map(gSavedSettings, "MiniMapRotate", true);

	LLColor4 laser_color = map_laser_color;

	if (mObjectImagep.isNull()) createObjectImage();
	if (auto_center) mCurPan = lerp(mCurPan, mTargetPan, LLCriticalDamp::getInterpolant(0.1f));

	F32 rotation = 0;

	gGL.pushMatrix();
	gGL.pushUIMatrix();

	LLVector3 offset = gGL.getUITranslation();
	LLVector3 scale = gGL.getUIScale();

	gGL.loadIdentity();
	gGL.loadUIIdentity();
	gGL.scalef(scale.mV[0], scale.mV[1], scale.mV[2]);
	gGL.translatef(offset.mV[0], offset.mV[1], offset.mV[2]);
	{
		LLLocalClipRect clip(getLocalRect());
		{
			gGL.getTexUnit(0)->unbind(LLTexUnit::TT_TEXTURE);
			gGL.matrixMode(LLRender::MM_MODELVIEW);

			// Draw background rectangle.
			LLColor4 background_color = mBackgroundColor.get();
			gGL.color4fv( background_color.mV );
			gl_rect_2d(0, getRect().getHeight(), getRect().getWidth(), 0);
		}

		// Region 0, 0 is in the middle.
		S32 center_sw_left = getRect().getWidth() / 2 + llfloor(mCurPan.mV[VX]);
		S32 center_sw_bottom = getRect().getHeight() / 2 + llfloor(mCurPan.mV[VY]);

		gGL.pushMatrix();
		gGL.translatef( (F32) center_sw_left, (F32) center_sw_bottom, 0.f);
		
		if (rotate_map)
		{
			// Rotate subsequent draws to agent rotation.
			rotation = atan2( LLViewerCamera::getInstance()->getAtAxis().mV[VX], LLViewerCamera::getInstance()->getAtAxis().mV[VY] );
			gGL.rotatef(rotation * RAD_TO_DEG, 0.f, 0.f, 1.f);
		}

		// Figure out where agent is.
		S32 region_width = llround(LLWorld::getInstance()->getRegionWidthInMeters());

		for (LLWorld::region_list_t::const_iterator iter = LLWorld::getInstance()->getRegionList().begin();
			 iter != LLWorld::getInstance()->getRegionList().end(); ++iter)
		{
			LLViewerRegion* regionp = *iter;

			// Find x and y position relative to camera's center.
			LLVector3 origin_agent = regionp->getOriginAgent();
			LLVector3 rel_region_pos = origin_agent - (center_to_region ?  region->getCenterAgent() : gAgentCamera.getCameraPositionAgent());
			F32 relative_x = (rel_region_pos.mV[0] / region_width) * mScale;
			F32 relative_y = (rel_region_pos.mV[1] / region_width) * mScale;

			// Background region rectangle.
			F32 bottom = relative_y;
			F32 left = relative_x;
			F32 top = bottom + mScale;
			F32 right = left + mScale;

			// <exodus>
			if (regionp == region) gGL.color4f(1.f, 1.f, 1.f, 0.75f);
			else if (!regionp->isAlive()) gGL.color4f(1.f, 0.5f, 0.5f, 0.5f);
			else gGL.color4f(0.75f, 0.75f, 0.75f, 0.5f);
			// </exodus>

			// Draw using texture.
			gGL.getTexUnit(0)->bind(regionp->getLand().getSTexture());
			gGL.begin(LLRender::QUADS);
				gGL.texCoord2f(0.f, 1.f);
				gGL.vertex2f(left, top);
				gGL.texCoord2f(0.f, 0.f);
				gGL.vertex2f(left, bottom);
				gGL.texCoord2f(1.f, 0.f);
				gGL.vertex2f(right, bottom);
				gGL.texCoord2f(1.f, 1.f);
				gGL.vertex2f(right, top);
			gGL.end();

			// Draw water
			gGL.setAlphaRejectSettings(LLRender::CF_GREATER, ABOVE_WATERLINE_ALPHA / 255.f);
			{
				if (regionp->getLand().getWaterTexture())
				{
					gGL.getTexUnit(0)->bind(regionp->getLand().getWaterTexture());
					gGL.begin(LLRender::QUADS);
						gGL.texCoord2f(0.f, 1.f);
						gGL.vertex2f(left, top);
						gGL.texCoord2f(0.f, 0.f);
						gGL.vertex2f(left, bottom);
						gGL.texCoord2f(1.f, 0.f);
						gGL.vertex2f(right, bottom);
						gGL.texCoord2f(1.f, 1.f);
						gGL.vertex2f(right, top);
					gGL.end();
				}
			}
			gGL.setAlphaRejectSettings(LLRender::CF_DEFAULT);
		}

		// Redraw object layer periodically
		if (mUpdateNow || (map_timer.getElapsedTimeF32() > 0.1f))
		{
			mUpdateNow = false;
			LLVector3 new_center = globalPosToView(center_to_region ? region->getCenterGlobal() : gAgentCamera.getCameraPositionGlobal());
			new_center.mV[VX] -= mCurPan.mV[VX];
			new_center.mV[VY] -= mCurPan.mV[VY];
			new_center.mV[VZ] = 0.f;
			mObjectImageCenterGlobal = viewPosToGlobal(llfloor(new_center.mV[VX]), llfloor(new_center.mV[VY]));
			U8 *default_texture = mObjectRawImagep->getData();
			memset(default_texture, 0, mObjectImagep->getWidth() * mObjectImagep->getHeight() * mObjectImagep->getComponents());
			
			static LLUICachedControl<bool> render_objects("ExodusMinimapRenderObjects", true);
			if (render_objects) gObjectList.renderObjectsForMap(*this);

			mObjectImagep->setSubImage(mObjectRawImagep, 0, 0, mObjectImagep->getWidth(), mObjectImagep->getHeight());
			map_timer.reset();
		}

		LLVector3 map_center_agent = gAgent.getPosAgentFromGlobal(mObjectImageCenterGlobal);

		map_center_agent -= center_to_region ? region->getCenterAgent() : gAgentCamera.getCameraPositionAgent();
		map_center_agent.mV[VX] *= mScale/region_width;
		map_center_agent.mV[VY] *= mScale/region_width;

		gGL.getTexUnit(0)->bind(mObjectImagep);

		F32 image_half_width = 0.5f*mObjectMapPixels;
		F32 image_half_height = 0.5f*mObjectMapPixels;

		gGL.begin(LLRender::QUADS);
			gGL.texCoord2f(0.f, 1.f);
			gGL.vertex2f(map_center_agent.mV[VX] - image_half_width, image_half_height + map_center_agent.mV[VY]);
			gGL.texCoord2f(0.f, 0.f);
			gGL.vertex2f(map_center_agent.mV[VX] - image_half_width, map_center_agent.mV[VY] - image_half_height);
			gGL.texCoord2f(1.f, 0.f);
			gGL.vertex2f(image_half_width + map_center_agent.mV[VX], map_center_agent.mV[VY] - image_half_height);
			gGL.texCoord2f(1.f, 1.f);
			gGL.vertex2f(image_half_width + map_center_agent.mV[VX], image_half_height + map_center_agent.mV[VY]);
		gGL.end();
		gGL.popMatrix();

		LLVector3d pos_global;
		LLVector3 pos_map;
		S32 local_mouse_x;
		S32 local_mouse_y;

		LLUI::getMousePositionLocal(this, &local_mouse_x, &local_mouse_y);

		F32 min_pick_dist = mDotRadius * mPickRadius;
		
		mClosestAgentToCursor.setNull();
		mClosestAgentsToCursor.clear();

		F32 closest_dist_squared = F32_MAX;
		F32 min_pick_dist_squared = (mDotRadius * MIN_PICK_SCALE) * (mDotRadius * MIN_PICK_SCALE);
		F32 meters_to_pixels = mScale / LLWorld::getInstance()->getRegionWidthInMeters();
		
		std::map<LLUUID, LLVector3d> local_positions;
			LLUUID uuid;
		
		for (LLWorld::region_list_t::const_iterator iter = LLWorld::getInstance()->getRegionList().begin();
			 iter != LLWorld::getInstance()->getRegionList().end(); ++iter)
		{
			LLViewerRegion* regionp = *iter;
			const LLVector3d& origin_global = regionp->getOriginGlobal();
			S32 count = regionp->mMapAvatars.count();
			U8 bits;
			U32 compact_local;
			LLVector3 pos_local;
			LLVector3d pos_global;

			for (S32 i = 0; i < count; i++)
			{
				compact_local = regionp->mMapAvatars.get(i);
				uuid = regionp->mMapAvatarIDs.get(i);

				bits = compact_local & 0xFF;
				pos_local.mV[VZ] = F32(bits) * 4.f;
				compact_local >>= 8;
				bits = compact_local & 0xFF;
				pos_local.mV[VY] = (F32)bits;
				compact_local >>= 8;
				bits = compact_local & 0xFF;
				pos_local.mV[VX] = (F32)bits;

				pos_global.setVec(pos_local);
				pos_global += origin_global;

				local_positions.insert(std::pair<LLUUID, LLVector3d>(uuid, pos_global));
			}
		}

		for (std::map<LLUUID, LLVector3d>::const_iterator iter = local_positions.begin();
			 iter != local_positions.end(); ++iter)
		{
			uuid = (*iter).first;
			pos_global = (*iter).second;

			BOOL show_as_friend = FALSE;
			LLColor4 color;
			BOOL height_unknown = (pos_global.mdV[VZ] == 0.f || pos_global.mdV[VZ] == 1020.f);
			F32 size_multiplier = 1.0f;
			F32 avatar_velocity = 0.f;
			LLViewerObject* avatar = gObjectList.findObject(uuid);

			if (avatar && avatar->mDrawable->isRecentlyVisible())
			{
				pos_global = avatar->getPositionGlobal();
				height_unknown = FALSE;

				static LLCachedControl<F32> velocity_multi(gSavedSettings, "ExodusMinimapVelocityMultiplier", 18.f);
				if (avatar->isSittingAvatar())
				{
					if (avatar->getParent())
					{
						avatar_velocity = ((LLViewerObject*)avatar->getParent())->getVelocity().magVec() / velocity_multi;
					}
					else
					{
						avatar_velocity = avatar->getVelocity().magVec() / velocity_multi;
					}
				}
				else
				{
					avatar_velocity = avatar->getVelocity().magVec() / velocity_multi;
				}

				if (avatar_velocity > 1.f)
				{
					avatar_velocity = 1.f;
				}
			}

			pos_map = globalPosToView(pos_global, true);

			static LLUICachedControl<bool> show_rotations("ExodusMinimapShowRotations", false);
			if (avatar && show_rotations)
			{
				if (rotate_map)
				{
					// Todo.
				}
				else
				{
					LLQuaternion avatar_rotation = avatar->getRotation();
					const LLQuaternion delta(1.0, 1.0, 0.0, 0.0);
					avatar_rotation = delta * avatar_rotation;
					F32 angle_radians, x, y, z;
					avatar_rotation.getAngleAxis(&angle_radians, &x, &y, &z);
					const F32 far_clip_pixels = 16.f * meters_to_pixels;
					const F32 half_width_pixels = (16.f * tan(1.5f / 2)) * meters_to_pixels;
					LLColor4 frustrum = map_frustum_color;

					gGL.getTexUnit(0)->unbind(LLTexUnit::TT_TEXTURE);
					gGL.color4fv(frustrum.mV);
					gGL.pushMatrix();
						gGL.translatef(pos_map.mV[VX], pos_map.mV[VY], 0);
						gGL.rotatef(angle_radians * RAD_TO_DEG, x, y, z);
						gGL.begin(LLRender::TRIANGLES);
							gGL.vertex2f(0, 0);
							frustrum.mV[VW] = 0.f;
							gGL.color4fv(frustrum.mV);
							gGL.vertex2f(-half_width_pixels, far_clip_pixels);
							gGL.vertex2f(half_width_pixels, far_clip_pixels);
						gGL.end();
					gGL.popMatrix();
				}
			}
				
			if (dist_vec(LLVector2(pos_map.mV[VX], pos_map.mV[VY]), LLVector2(local_mouse_x, local_mouse_y)) < min_pick_dist)
			{
				mClosestAgentsToCursor[uuid] = pos_global;
				color = map_avatar_rollover_color;
			}
			else
			{
				show_as_friend = (LLAvatarTracker::instance().getBuddyInfo(uuid) != NULL);
				std::string full_name, last_name;
				gCacheName->getFullName(uuid, full_name);
				LLMuteList* muteListInstance = LLMuteList::getInstance();
				BOOL is_linden = (full_name.find("Linden") != std::string::npos);

				color = muteListInstance->isMuted(uuid) ? LLColor4::grey :
					show_as_friend ? map_avatar_friend_color : (is_linden ? map_linden_color : map_avatar_color);

				if (!avatarColors.empty())
				{
					std::map<LLUUID, LLColor4>::iterator it = avatarColors.find(uuid);
					if (it != avatarColors.end()) color = it->second;
				}

				static LLUICachedControl<bool> interpolated_color("ExodusMinimapInterpolateColor", false);
				if (interpolated_color && avatar_velocity > 0.f)
				{
					color = lerp(color, LLColor4::white, avatar_velocity / 1.25f); // 75% white at 32m/s (default).
				}

				static LLUICachedControl<bool> interpolated_size("ExodusMinimapInterpolateSize", false);
				if (interpolated_size && avatar_velocity > 0.f)
				{
					static LLCachedControl<F32> size_multi(gSavedSettings, "ExodusMinimapSizeMultiplier", 2.f);
					size_multiplier += (avatar_velocity / size_multi); // 50% extra size at 32m/s (default).
				}
			}

			LLWorldMapView::drawAvatar(
				pos_map.mV[VX], pos_map.mV[VY], 
				color, 
				pos_map.mV[VZ], mDotRadius * size_multiplier,
				height_unknown);

			BOOL selected = FALSE;
			uuid_vec_t::iterator sel_iter = gmSelected.begin();

			for (; sel_iter != gmSelected.end(); sel_iter++)
			{
				if (*sel_iter == uuid)
				{
					selected = TRUE;
					break;
				}
			}

			if (selected)
			{
				if ((pos_map.mV[VX] < 0) ||
					(pos_map.mV[VY] < 0) ||
					(pos_map.mV[VX] >= getRect().getWidth()) ||
					(pos_map.mV[VY] >= getRect().getHeight()))
				{
					S32 x = llround(pos_map.mV[VX]);
					S32 y = llround(pos_map.mV[VY]);

					LLWorldMapView::drawTrackingCircle(getRect(), x, y, color, 1, 10);
				}
				else LLWorldMapView::drawTrackingDot(pos_map.mV[VX],pos_map.mV[VY],color,0.f);
			}

			F32	dist_to_cursor_squared = dist_vec_squared(LLVector2(pos_map.mV[VX], pos_map.mV[VY]), LLVector2(local_mouse_x,local_mouse_y));
			if (dist_to_cursor_squared < min_pick_dist_squared && dist_to_cursor_squared < closest_dist_squared)
			{
				closest_dist_squared = dist_to_cursor_squared;
				mClosestAgentToCursor = uuid;
			}
		}

		if (gAgent.getAutoPilot())
		{
			drawTracking(gAgent.getAutoPilotTargetGlobal(), map_track_color);
		}
		else
		{
			LLTracker::ETrackingStatus tracking_status = LLTracker::getTrackingStatus();
			if (LLTracker::TRACKING_AVATAR == tracking_status)
			{
				drawTracking(LLAvatarTracker::instance().getGlobalPos(), map_track_color);
			} 
			else if (LLTracker::TRACKING_LANDMARK == tracking_status ||
					 LLTracker::TRACKING_LOCATION == tracking_status)
			{
				drawTracking(LLTracker::getTrackedPositionGlobal(), map_track_color);
			}
		}

		pos_map = globalPosToView(gAgent.getPositionGlobal());
		S32 dot_width = llround(mDotRadius * 2.f);
		LLUIImagePtr you = LLWorldMapView::sAvatarYouLargeImage;

		if (you)
		{
			you->draw(llround(pos_map.mV[VX] - mDotRadius),
					  llround(pos_map.mV[VY] - mDotRadius),
					  dot_width,
					  dot_width);

			F32	dist_to_cursor_squared = dist_vec_squared(LLVector2(pos_map.mV[VX], pos_map.mV[VY]),
										  LLVector2(local_mouse_x,local_mouse_y));

			if (dist_to_cursor_squared < min_pick_dist_squared && dist_to_cursor_squared < closest_dist_squared)
			{
				mClosestAgentToCursor = gAgent.getID();
			}
		}

		F32 horiz_fov = LLViewerCamera::getInstance()->getView() * LLViewerCamera::getInstance()->getAspect();
		F32 far_clip_meters = LLViewerCamera::getInstance()->getFar() + 10.f;

		if (far_clip_meters > 180.f) far_clip_meters = 180.f;

		F32 far_clip_pixels = far_clip_meters * meters_to_pixels;
		F32 half_width_meters = far_clip_meters * tan( horiz_fov / 2 );
		F32 half_width_pixels = half_width_meters * meters_to_pixels;
		F32 ctr_x, ctr_y;
		
		if (center_to_region)
		{
			LLVector3 camera_position = globalPosToView(gAgentCamera.getCameraPositionGlobal());

			ctr_x = camera_position.mV[VX];
			ctr_y = camera_position.mV[VY];
		}
		else
		{
			ctr_x = (F32)center_sw_left;
			ctr_y = (F32)center_sw_bottom;
		}

		gGL.getTexUnit(0)->unbind(LLTexUnit::TT_TEXTURE);

		LLColor4 frustrum = map_frustum_color;
		gGL.color4fv(frustrum.mV);

		// <exodus> Draw mouse radius
		// Todo: Detect if over the window and don't render a circle?
		gl_circle_2d(local_mouse_x, local_mouse_y, min_pick_dist, 32, true);
		// </exodus>

		if (rotate_map)
		{
			gGL.begin(LLRender::TRIANGLES);
				gGL.vertex2f(ctr_x, ctr_y);
				frustrum.mV[VW] = 0.f;
				gGL.color4fv(frustrum.mV);
				gGL.vertex2f(ctr_x - half_width_pixels, ctr_y + far_clip_pixels);
				gGL.vertex2f(ctr_x + half_width_pixels, ctr_y + far_clip_pixels);
			gGL.end();
			gGL.begin(LLRender::LINES);
				gGL.color4fv(laser_color.mV);
				gGL.vertex2f(ctr_x + 0.82f, ctr_y + 2.5f);
				laser_color.mV[VW] = 0.f;
				gGL.color4fv(laser_color.mV);
				gGL.vertex2f(ctr_x + 0.82f, ctr_y + (far_clip_pixels * 2.f));
			gGL.end();

			if (map_projectial_trajectory && gAgentCamera.cameraMouselook())
			{
				F32 pitch = LLViewerCamera::getInstance()->getPitch();
				F32 vertical = sinf(pitch) * map_projectial_velocity;
				F32 distance = (cosf(pitch) * map_projectial_velocity) * (vertical / map_projectial_buoyancy);

				if (distance > 0.f)
				{
					distance *= meters_to_pixels;

					gGL.begin(LLRender::LINES);
						gGL.color4fv(LLColor4(map_projectial_line_color).mV);
						gGL.vertex2f(ctr_x + 0.82f, ctr_y + 2.5f);
						gGL.vertex2f(ctr_x + 0.82f, ctr_y + distance);
						gGL.vertex2f(ctr_x - 2.82f, ctr_y + distance);
						gGL.vertex2f(ctr_x + 3.82f, ctr_y + distance);
					gGL.end();
				}
			}
		}
		else
		{
			gGL.pushMatrix();
				gGL.translatef(ctr_x, ctr_y, 0);
				gGL.rotatef(atan2( LLViewerCamera::getInstance()->getAtAxis().mV[VX], LLViewerCamera::getInstance()->getAtAxis().mV[VY] ) * RAD_TO_DEG, 0.f, 0.f, -1.f);
				gGL.begin(LLRender::TRIANGLES);
					gGL.vertex2f(0.f, 0.f);
					frustrum.mV[VW] = 0.f;
					gGL.color4fv(frustrum.mV);
					gGL.vertex2f(-half_width_pixels, far_clip_pixels);
					gGL.vertex2f( half_width_pixels, far_clip_pixels);
				gGL.end();
				gGL.begin(LLRender::LINES);
					gGL.color4fv(laser_color.mV);
					gGL.vertex2f(0.82f, 2.5f);
					laser_color.mV[VW] = 0.f;
					gGL.color4fv(laser_color.mV);
					gGL.vertex2f(0.82f, far_clip_pixels * 1.25f);
				gGL.end();

				if (map_projectial_trajectory && gAgentCamera.cameraMouselook())
				{
					F32 pitch = LLViewerCamera::getInstance()->getPitch();
					F32 vertical = sinf(pitch) * map_projectial_velocity;
					F32 distance = (cosf(pitch) * map_projectial_velocity) * (vertical / map_projectial_buoyancy);

					if (distance > 0.f)
					{
						distance *= meters_to_pixels;

						gGL.begin(LLRender::LINES);
							gGL.color4fv(LLColor4(map_projectial_line_color).mV);
							gGL.vertex2f(0.82f, 2.5f);
							gGL.vertex2f(0.82f, distance);
							gGL.vertex2f(-2.82f, distance);
							gGL.vertex2f(3.82f, distance);
						gGL.end();
					}
				}

			gGL.popMatrix();
		}
	}
	
	gGL.popMatrix();
	gGL.popUIMatrix();

	LLUICtrl::draw();
}
// </exodus>

void LLNetMap::reshape(S32 width, S32 height, BOOL called_from_parent)
{
	LLUICtrl::reshape(width, height, called_from_parent);
	createObjectImage();
}

// <exodus>
LLVector3 LLNetMap::globalPosToView( const LLVector3d& global_pos, bool agent_pos )
{
	static LLUICachedControl<bool> center_to_region("ExodusMinimapCenter", false);

	LLVector3d relative_pos_global = global_pos;
	LLViewerRegion* region = gAgent.getRegion();
	
	if (center_to_region && agent_pos && region)
	{
		LLVector3d region_center_global = region->getCenterGlobal();
		relative_pos_global.mdV[VX] -= region_center_global.mdV[VX];
		relative_pos_global.mdV[VY] -= region_center_global.mdV[VY];
		relative_pos_global.mdV[VZ] -= gAgentCamera.getCameraPositionGlobal().mdV[VZ];
	}
	else relative_pos_global -= center_to_region && region ? region->getCenterGlobal() : gAgentCamera.getCameraPositionGlobal();

	LLVector3 pos_local;
	pos_local.setVec(relative_pos_global);  // convert to floats from doubles

	pos_local.mV[VX] *= mPixelsPerMeter;
	pos_local.mV[VY] *= mPixelsPerMeter;
	// leave Z component in meters

	static LLUICachedControl<bool> rotate_map("MiniMapRotate", true);
	if (rotate_map)
	{
		F32 radians = atan2(LLViewerCamera::getInstance()->getAtAxis().mV[VX], LLViewerCamera::getInstance()->getAtAxis().mV[VY]);
		LLQuaternion rot(radians, LLVector3(0.f, 0.f, 1.f));
		pos_local.rotVec(rot); // <exodus/>
	}

	pos_local.mV[VX] += getRect().getWidth() / 2 + mCurPan.mV[VX];
	pos_local.mV[VY] += getRect().getHeight() / 2 + mCurPan.mV[VY];

	return pos_local;
}

void LLNetMap::drawTracking(const LLVector3d& pos_global, const LLColor4& color, 
							BOOL draw_arrow )
{
	LLVector3 pos_local = globalPosToView( pos_global );
	if ((pos_local.mV[VX] < 0) ||
		(pos_local.mV[VY] < 0) ||
		(pos_local.mV[VX] >= getRect().getWidth()) ||
		(pos_local.mV[VY] >= getRect().getHeight()))
	{
		if (draw_arrow)
		{
			S32 x = llround( pos_local.mV[VX] );
			S32 y = llround( pos_local.mV[VY] );
			LLWorldMapView::drawTrackingCircle( getRect(), x, y, color, 1, 10 );
			LLWorldMapView::drawTrackingArrow( getRect(), x, y, color );
		}
	}
	else
	{
		LLWorldMapView::drawTrackingDot(pos_local.mV[VX], 
										pos_local.mV[VY], 
										color,
										pos_local.mV[VZ]);
	}
}

LLVector3d LLNetMap::viewPosToGlobal( S32 x, S32 y )
{
	x -= llround(getRect().getWidth() / 2 + mCurPan.mV[VX]);
	y -= llround(getRect().getHeight() / 2 + mCurPan.mV[VY]);

	LLVector3 pos_local( (F32)x, (F32)y, 0 );

	F32 radians = - atan2( LLViewerCamera::getInstance()->getAtAxis().mV[VX], LLViewerCamera::getInstance()->getAtAxis().mV[VY] );

	static LLUICachedControl<bool> rotate_map("MiniMapRotate", true);
	if (rotate_map)
	{
		LLQuaternion rot(radians, LLVector3(0.f, 0.f, 1.f));
		pos_local.rotVec( rot );
	}

	pos_local *= ( LLWorld::getInstance()->getRegionWidthInMeters() / mScale );
	
	LLVector3d pos_global;
	pos_global.setVec( pos_local );

	// <exodus>
	static LLUICachedControl<bool> center_to_region("ExodusMinimapCenter", false);

	pos_global += center_to_region && gAgent.getRegion() ? gAgent.getRegion()->getCenterGlobal() : gAgentCamera.getCameraPositionGlobal();
	// </exodus>

	return pos_global;
}

BOOL LLNetMap::handleScrollWheel(S32 x, S32 y, S32 clicks)
{
	// <exodus>
	if (gKeyboard->currentMask(TRUE) & MASK_SHIFT)
	{
		mPickRadius = llclamp(mPickRadius + (2.5f * clicks), 1.f, 64.f);

		gSavedSettings.setF32("ExodusMinimapAreaEffect", mPickRadius);
	}
	else
	{
		// note that clicks are reversed from what you'd think: i.e. > 0  means zoom out, < 0 means zoom in
		F32 new_scale = mScale * pow(MAP_SCALE_ZOOM_FACTOR, -clicks);
		F32 old_scale = mScale;

		setScale(new_scale);

		static LLUICachedControl<bool> auto_center("MiniMapAutoCenter", true);
		if (!auto_center)
		{
			// Adjust pan to center the zoom on the mouse pointer
			LLVector2 zoom_offset;
			zoom_offset.mV[VX] = x - getRect().getWidth() / 2;
			zoom_offset.mV[VY] = y - getRect().getHeight() / 2;
			mCurPan -= zoom_offset * mScale / old_scale - zoom_offset;
		}
	}
	// <exodus/>

	return TRUE;
}

// <exodus>
BOOL LLNetMap::handleToolTip( S32 x, S32 y, MASK mask )
{
	if (gDisconnected)
	{
		return FALSE;
	}

	// If the cursor is near an avatar on the minimap, a mini-inspector will be
	// shown for the avatar, instead of the normal map tooltip.
    if (handleToolTipAgent(mClosestAgentToCursor))
	{
		return TRUE;
	}

	LLRect sticky_rect;
	std::string tool_tip;

	LLViewerRegion*	region = LLWorld::getInstance()->getRegionFromPosGlobal( viewPosToGlobal( x, y ) );
	if (region)
	{
		// set sticky_rect
		S32 SLOP = 4;
		localPointToScreen(x - SLOP, y - SLOP, &(sticky_rect.mLeft), &(sticky_rect.mBottom));
		sticky_rect.mRight = sticky_rect.mLeft + 2 * SLOP;
		sticky_rect.mTop = sticky_rect.mBottom + 2 * SLOP;

		tool_tip = region->getName();

		if (region->mMapAvatarIDs.count())
		{
			if (mClosestAgentsToCursor.size())
			{
				tool_tip.append(llformat(" (%d/%d)\n", mClosestAgentsToCursor.size(), region->mMapAvatarIDs.count() + 1));

				LLVector3d myPosition = gAgent.getPositionGlobal();

				std::map<LLUUID, LLVector3d>::iterator current = mClosestAgentsToCursor.begin();
				std::map<LLUUID, LLVector3d>::iterator end = mClosestAgentsToCursor.end();
				for (; current != end; ++current)
				{
					LLUUID targetUUID = (*current).first;
					LLVector3d targetPosition = (*current).second;

					std::string fullName;
					if (gCacheName->getFullName(targetUUID, fullName))
					{
						tool_tip.append("\n");
						tool_tip.append(fullName);

						if (targetPosition.mdV[VZ])
						{
							LLVector3d delta = targetPosition - myPosition;
							F32 distance = (F32)delta.magVec();
							tool_tip.append(llformat(" (%.02fm)", distance));
						}
					}
				}
			}
			else tool_tip.append(llformat(" (%d)", region->mMapAvatarIDs.count() + 1));
		}

		if (tool_tip.empty()) return TRUE;

		LLToolTipMgr::instance().show(LLToolTip::Params().message(tool_tip).sticky_rect(sticky_rect));
	}

	return TRUE;
}
// </exodus>

BOOL LLNetMap::handleToolTipAgent(const LLUUID& avatar_id)
{
	LLAvatarName av_name;
	if (avatar_id.isNull() || !LLAvatarNameCache::get(avatar_id, &av_name))
	{
		return FALSE;
	}

	// only show tooltip if same inspector not already open
	LLFloater* existing_inspector = LLFloaterReg::findInstance("inspect_avatar");
	if (!existing_inspector
		|| !existing_inspector->getVisible()
		|| existing_inspector->getKey()["avatar_id"].asUUID() != avatar_id)
	{
		LLInspector::Params p;
		p.fillFrom(LLUICtrlFactory::instance().getDefaultParams<LLInspector>());
		p.message(av_name.getCompleteName());
		p.image.name("Inspector_I");
		p.click_callback(boost::bind(showAvatarInspector, avatar_id));
		p.visible_time_near(6.f);
		p.visible_time_far(3.f);
		p.delay_time(0.35f);
		p.wrap(false);

		LLToolTipMgr::instance().show(p);
	}
	return TRUE;
}

// static
void LLNetMap::showAvatarInspector(const LLUUID& avatar_id)
{
	LLSD params;
	params["avatar_id"] = avatar_id;

	if (LLToolTipMgr::instance().toolTipVisible())
	{
		LLRect rect = LLToolTipMgr::instance().getToolTipRect();
		params["pos"]["x"] = rect.mLeft;
		params["pos"]["y"] = rect.mTop;
	}

	LLFloaterReg::showInstance("inspect_avatar", params);
}

void LLNetMap::renderScaledPointGlobal( const LLVector3d& pos, const LLColor4U &color, F32 radius_meters )
{
	LLVector3 local_pos;
	local_pos.setVec( pos - mObjectImageCenterGlobal );

	S32 diameter_pixels = llround(2 * radius_meters * mObjectMapTPM);
	renderPoint( local_pos, color, diameter_pixels );
}


void LLNetMap::renderPoint(const LLVector3 &pos_local, const LLColor4U &color, 
						   S32 diameter, S32 relative_height)
{
	if (diameter <= 0)
	{
		return;
	}

	const S32 image_width = (S32)mObjectImagep->getWidth();
	const S32 image_height = (S32)mObjectImagep->getHeight();

	S32 x_offset = llround(pos_local.mV[VX] * mObjectMapTPM + image_width / 2);
	S32 y_offset = llround(pos_local.mV[VY] * mObjectMapTPM + image_height / 2);

	if ((x_offset < 0) || (x_offset >= image_width))
	{
		return;
	}
	if ((y_offset < 0) || (y_offset >= image_height))
	{
		return;
	}

	U8 *datap = mObjectRawImagep->getData();

	S32 neg_radius = diameter / 2;
	S32 pos_radius = diameter - neg_radius;
	S32 x, y;

	if (relative_height > 0)
	{
		// ...point above agent
		S32 px, py;

		// vertical line
		px = x_offset;
		for (y = -neg_radius; y < pos_radius; y++)
		{
			py = y_offset + y;
			if ((py < 0) || (py >= image_height))
			{
				continue;
			}
			S32 offset = px + py * image_width;
			((U32*)datap)[offset] = color.mAll;
		}

		// top line
		py = y_offset + pos_radius - 1;
		for (x = -neg_radius; x < pos_radius; x++)
		{
			px = x_offset + x;
			if ((px < 0) || (px >= image_width))
			{
				continue;
			}
			S32 offset = px + py * image_width;
			((U32*)datap)[offset] = color.mAll;
		}
	}
	else
	{
		// ...point level with agent
		for (x = -neg_radius; x < pos_radius; x++)
		{
			S32 p_x = x_offset + x;
			if ((p_x < 0) || (p_x >= image_width))
			{
				continue;
			}

			for (y = -neg_radius; y < pos_radius; y++)
			{
				S32 p_y = y_offset + y;
				if ((p_y < 0) || (p_y >= image_height))
				{
					continue;
				}
				S32 offset = p_x + p_y * image_width;
				((U32*)datap)[offset] = color.mAll;
			}
		}
	}
}

void LLNetMap::createObjectImage()
{
	// Find the size of the side of a square that surrounds the circle that surrounds getRect().
	// ... which is, the diagonal of the rect.
	F32 width = (F32)getRect().getWidth();
	F32 height = (F32)getRect().getHeight();
	S32 square_size = llround( sqrt(width*width + height*height) );

	// Find the least power of two >= the minimum size.
	const S32 MIN_SIZE = 64;
	const S32 MAX_SIZE = 256;
	S32 img_size = MIN_SIZE;
	while( (img_size*2 < square_size ) && (img_size < MAX_SIZE) )
	{
		img_size <<= 1;
	}

	if( mObjectImagep.isNull() ||
		(mObjectImagep->getWidth() != img_size) ||
		(mObjectImagep->getHeight() != img_size) )
	{
		mObjectRawImagep = new LLImageRaw(img_size, img_size, 4);
		U8* data = mObjectRawImagep->getData();
		memset( data, 0, img_size * img_size * 4 );
		mObjectImagep = LLViewerTextureManager::getLocalTexture( mObjectRawImagep.get(), FALSE);
	}
	setScale(mScale);
	mUpdateNow = true;
}

BOOL LLNetMap::handleMouseDown( S32 x, S32 y, MASK mask )
{
	if (!(mask & MASK_SHIFT)) return FALSE;

	// Start panning
	gFocusMgr.setMouseCapture(this);

	mStartPan = mCurPan;
	mMouseDown.mX = x;
	mMouseDown.mY = y;
	return TRUE;
}

BOOL LLNetMap::handleMouseUp( S32 x, S32 y, MASK mask )
{
	if(abs(mMouseDown.mX-x)<3 && abs(mMouseDown.mY-y)<3)
		handleClick(x,y,mask);

	if (hasMouseCapture())
	{
		if (mPanning)
		{
			// restore mouse cursor
			S32 local_x, local_y;
			local_x = mMouseDown.mX + llfloor(mCurPan.mV[VX] - mStartPan.mV[VX]);
			local_y = mMouseDown.mY + llfloor(mCurPan.mV[VY] - mStartPan.mV[VY]);
			LLRect clip_rect = getRect();
			clip_rect.stretch(-8);
			clip_rect.clipPointToRect(mMouseDown.mX, mMouseDown.mY, local_x, local_y);
			LLUI::setMousePositionLocal(this, local_x, local_y);

			// finish the pan
			mPanning = false;

			mMouseDown.set(0, 0);

			// auto centre
			mTargetPan.setZero();
		}
		gViewerWindow->showCursor();
		gFocusMgr.setMouseCapture(NULL);
		return TRUE;
	}
	return FALSE;
}

// <exodus>
BOOL LLNetMap::handleRightMouseDown(S32 x, S32 y, MASK mask)
{
	mClosestAgentsAtLastClick = mClosestAgentsToCursor;
	
	LLColor4 customColor = gSavedSettings.getColor4("ExodusMinimapMarkCustom");
	LLColor4 hostileColor = gSavedSettings.getColor4("ExodusMapAvatarColor");
	LLColor4 friendlyColor = gSavedSettings.getColor4("ExodusMapAvatarFriendColor");
	
	// Yes, I think I am doing it right? :O
	// Not sure what the best way to do this is, couldn't find any good examples elsewhere.

	if(mClosestAgentsToCursor.size() > 1)
	{
		LLUICtrl::CommitCallbackRegistry::ScopedRegistrar registrar;
		registrar.add("Minimap.Zoom", boost::bind(&LLNetMap::handleZoom, this, _2));
		registrar.add("Minimap.Tracker", boost::bind(&LLNetMap::handleStopTracking, this, _2));
	
		LLMenuGL* coreMenu = LLUICtrlFactory::getInstance()->createFromFile<LLMenuGL>("exo_menu_mini_map_multiple.xml", gMenuHolder, LLViewerMenuHolderGL::child_registry_t::instance());

		coreMenu->updateParent(LLMenuGL::sMenuContainer);
		coreMenu->setCanTearOff(true);
		coreMenu->setItemEnabled("Target Name", false);
		coreMenu->setItemFont("Target Name", LLFontGL::getFontSansSerifBold());

		std::map<LLUUID, LLVector3d>::iterator current = mClosestAgentsToCursor.begin();
		std::map<LLUUID, LLVector3d>::iterator end = mClosestAgentsToCursor.end();
		for(; current != end; ++current)
		{
			LLUUID targetUUID = (*current).first;
			LLVector3d targetPosition = (*current).second;
			LLViewerObject *targetObject = gObjectList.findObject(targetUUID);

			std::string menuName;

			bool is_not_null = targetObject == NULL ? false : true; 
			bool show_as_friend = (LLAvatarTracker::instance().getBuddyInfo(targetUUID) != NULL);
			if(!gCacheName->getFullName(targetUUID, menuName)) menuName = "Loading...";

			LLMenuGL* currentMenu = LLUICtrlFactory::getInstance()->createFromFile<LLMenuGL>("exo_menu_mini_map_agent.xml", gMenuHolder, LLViewerMenuHolderGL::child_registry_t::instance());

			currentMenu->setItemCommitCallback("Profile", boost::bind(&LLNetMap::openProfile, targetUUID, menuName));
			if(show_as_friend)currentMenu->setItemVisible("Add Friend", false);
			else currentMenu->setItemCommitCallback("Add Friend", boost::bind(&LLNetMap::addFriend, targetUUID, menuName));
			currentMenu->setItemCommitCallback("Instant Message", boost::bind(&LLNetMap::instantMessage, targetUUID, menuName));
			currentMenu->setItemEnabled("Zoom In", is_not_null);
			currentMenu->setItemCommitCallback("Zoom In", boost::bind(&LLNetMap::zoomIn, targetUUID, menuName));
			
			const BOOL enabled = exoSystems::databaseBackendLocation.length() > 0;
			if (enabled) currentMenu->setItemCommitCallback("Axis", boost::bind(&LLNetMap::openAxis, targetUUID, menuName));
			currentMenu->setItemLabel("Axis", (exoSystems::databaseBackendLabel.length() > 0 ? exoSystems::databaseBackendLabel : "Axis") + "...");
			currentMenu->setItemVisible("Axis", enabled);

			currentMenu->setItemCommitCallback("Teleport To", boost::bind(&LLNetMap::teleportTo, targetUUID, targetPosition));
			currentMenu->setItemEnabled("Teleport To", is_not_null);
			currentMenu->setItemColor("Teleport To", LLColor4::yellow);

			currentMenu->setItemCommitCallback("Offer Teleport", boost::bind(&LLNetMap::offerTeleport, targetUUID, menuName));
			currentMenu->setItemColor("Offer Teleport", LLColor4::green);

			currentMenu->setItemEnabled("Request Teleport", show_as_friend);
			currentMenu->setItemCommitCallback("Request Teleport", boost::bind(&LLNetMap::requestTeleport, targetUUID, menuName));
			currentMenu->setItemColor("Request Teleport", LLColor4::cyan);

			LLMenuGL* subMenu = currentMenu->getChild<LLMenuItemBranchGL>("Moderation")->getBranch();
			if(subMenu)
			{
				bool can_freeze = LLAvatarActions::canLandFreezeOrEject(targetUUID);
				subMenu->setItemEnabled("Parcel Freeze", can_freeze);
				subMenu->setItemCommitCallback("Parcel Freeze", boost::bind(&LLNetMap::moderationTools, targetUUID, "parcel freeze"));
				subMenu->setItemEnabled("Parcel Eject/Ban", can_freeze);
				subMenu->setItemCommitCallback("Parcel Eject/Ban", boost::bind(&LLNetMap::moderationTools, targetUUID, "parcel eject/ban"));

				bool can_estate = LLAvatarActions::canEstateKickOrTeleportHome(targetUUID);
				subMenu->setItemEnabled("Estate Kill", can_estate);
				subMenu->setItemCommitCallback("Estate Kill", boost::bind(&LLNetMap::moderationTools, targetUUID, "teleport home"));
				subMenu->setItemEnabled("Estate Kick/Ban", can_estate);
				subMenu->setItemCommitCallback("Estate Kick/Ban", boost::bind(&LLNetMap::moderationTools, targetUUID, "estate kick/ban"));

				subMenu->setItemCommitCallback("Report", boost::bind(&LLNetMap::moderationTools, targetUUID, "report"));
			}
			
			subMenu = currentMenu->getChild<LLMenuItemBranchGL>("Mark")->getBranch();
			if(subMenu)
			{
				subMenu->setItemColor("Mark Friendly", friendlyColor);
				subMenu->setItemCommitCallback("Mark Friendly", boost::bind(&LLNetMap::markAgent, targetUUID, friendlyColor));
				subMenu->setItemColor("Mark Hostile", hostileColor);
				subMenu->setItemCommitCallback("Mark Hostile", boost::bind(&LLNetMap::markAgent, targetUUID, hostileColor));

				subMenu->setItemColor("Mark Red", LLColor4::red);
				subMenu->setItemCommitCallback("Mark Red", boost::bind(&LLNetMap::markAgent, targetUUID, LLColor4::red));
				subMenu->setItemColor("Mark Green", LLColor4::green);
				subMenu->setItemCommitCallback("Mark Green", boost::bind(&LLNetMap::markAgent, targetUUID, LLColor4::green));
				subMenu->setItemColor("Mark Cyan", LLColor4::cyan);
				subMenu->setItemCommitCallback("Mark Cyan", boost::bind(&LLNetMap::markAgent, targetUUID, LLColor4::cyan));
				subMenu->setItemColor("Mark Yellow", LLColor4::yellow);
				subMenu->setItemCommitCallback("Mark Yellow", boost::bind(&LLNetMap::markAgent, targetUUID, LLColor4::yellow));

				subMenu->setItemColor("Mark Custom", customColor);
				subMenu->setItemCommitCallback("Mark Custom", boost::bind(&LLNetMap::markAgent, targetUUID, customColor));

				subMenu->setItemCommitCallback("Mark Remove", boost::bind(&LLNetMap::markAgentRemove, targetUUID, menuName));
			}

			currentMenu->mCustomColor = show_as_friend ? LLColor4(0.25, 1.0, 1.0) : LLColor4(); // Maybe this should be set to marked color in future?

			currentMenu->buildDrawLabels();
			currentMenu->updateParent(LLMenuGL::sMenuContainer);
			currentMenu->setName(menuName);
			currentMenu->setLabel(menuName);
			currentMenu->setCanTearOff(true);

			coreMenu->appendMenu(currentMenu);
		}

		coreMenu->addSeparator();
		coreMenu->appendMenu(menuMinimap);

		registrar.add("Minimap.Mark.Multiple.Friendly", boost::bind(&LLNetMap::markMassAgents, friendlyColor));
		registrar.add("Minimap.Mark.Multiple.Hostile", boost::bind(&LLNetMap::markMassAgents, hostileColor));
		registrar.add("Minimap.Mark.Multiple.Red", boost::bind(&LLNetMap::markMassAgents, LLColor4::red));
		registrar.add("Minimap.Mark.Multiple.Green", boost::bind(&LLNetMap::markMassAgents, LLColor4::green));
		registrar.add("Minimap.Mark.Multiple.Cyan", boost::bind(&LLNetMap::markMassAgents, LLColor4::cyan));
		registrar.add("Minimap.Mark.Multiple.Yellow", boost::bind(&LLNetMap::markMassAgents, LLColor4::yellow));
		registrar.add("Minimap.Mark.Multiple.Custom", boost::bind(&LLNetMap::markMassAgents, customColor));
		registrar.add("Minimap.Mark.Multiple.Remove", boost::bind(&LLNetMap::markMassAgentsRemove));
		registrar.add("Minimap.Mark.All.Remove", boost::bind(&LLNetMap::markClear));

		LLMenuGL* menuMarkAll = LLUICtrlFactory::getInstance()->createFromFile<LLMenuGL>("exo_menu_mini_map_mark_all.xml", gMenuHolder, LLViewerMenuHolderGL::child_registry_t::instance());
	
		menuMarkAll->setItemColor("Marked Friendly", friendlyColor);
		menuMarkAll->setItemColor("Marked Hostile", hostileColor);

		menuMarkAll->setItemColor("Marked Red", LLColor4::red);
		menuMarkAll->setItemColor("Marked Green", LLColor4::green);
		menuMarkAll->setItemColor("Marked Cyan", LLColor4::cyan);
		menuMarkAll->setItemColor("Marked Yellow", LLColor4::yellow);

		menuMarkAll->setItemColor("Marked Custom", customColor);

		coreMenu->appendMenu(menuMarkAll);
		coreMenu->buildDrawLabels();

		LLMenuGL::showPopup(this, coreMenu, x, y);
	}
	else if(!mClosestAgentsToCursor.empty())
	{
		std::map<LLUUID, LLVector3d>::iterator current = mClosestAgentsToCursor.begin();

		LLUUID targetUUID = (*current).first;
		LLVector3d targetPosition = (*current).second;
		LLViewerObject *targetObject = gObjectList.findObject(targetUUID);

		std::string menuName;

		bool is_not_null = targetObject == NULL ? false : true; 
		bool show_as_friend = (LLAvatarTracker::instance().getBuddyInfo(targetUUID) != NULL);
		if(!gCacheName->getFullName(targetUUID, menuName)) menuName = "Loading...";
		
		LLUICtrl::CommitCallbackRegistry::ScopedRegistrar registrar;

		registrar.add("Minimap.Profile", boost::bind(&LLNetMap::openProfile, targetUUID, menuName));
		registrar.add("Minimap.Axis", boost::bind(&LLNetMap::openAxis, targetUUID, menuName));
		registrar.add("Minimap.Add.Friend", boost::bind(&LLNetMap::addFriend, targetUUID, menuName));
		registrar.add("Minimap.Instant.Message", boost::bind(&LLNetMap::instantMessage, targetUUID, menuName));
		registrar.add("Minimap.Zoom.In", boost::bind(&LLNetMap::zoomIn, targetUUID, menuName));
		registrar.add("Minimap.Offer.Teleport", boost::bind(&LLNetMap::offerTeleport, targetUUID, menuName));
		registrar.add("Minimap.Request.Teleport", boost::bind(&LLNetMap::requestTeleport, targetUUID, menuName));
		registrar.add("Minimap.Teleport.To", boost::bind(&LLNetMap::teleportTo, targetUUID, targetPosition));
		registrar.add("Minimap.Tracker", boost::bind(&LLNetMap::handleStopTracking, this, _2));
		registrar.add("Minimap.Parcel.Freeze", boost::bind(&LLNetMap::moderationTools, targetUUID, "parcel freeze"));
		registrar.add("Minimap.Parcel.Eject", boost::bind(&LLNetMap::moderationTools, targetUUID, "parcel eject/ban"));
		registrar.add("Minimap.Estate.Kill", boost::bind(&LLNetMap::moderationTools, targetUUID, "teleport home"));
		registrar.add("Minimap.Estate.Kick", boost::bind(&LLNetMap::moderationTools, targetUUID, "estate kick/ban"));
		registrar.add("Minimap.Report", boost::bind(&LLNetMap::moderationTools, targetUUID, "report"));
		registrar.add("Minimap.Mark.Single.Friendly", boost::bind(&LLNetMap::markAgent, targetUUID, friendlyColor));
		registrar.add("Minimap.Mark.Single.Hostile", boost::bind(&LLNetMap::markAgent, targetUUID, hostileColor));
		registrar.add("Minimap.Mark.Single.Red", boost::bind(&LLNetMap::markAgent, targetUUID, LLColor4::red));
		registrar.add("Minimap.Mark.Single.Green", boost::bind(&LLNetMap::markAgent, targetUUID, LLColor4::green));
		registrar.add("Minimap.Mark.Single.Cyan", boost::bind(&LLNetMap::markAgent, targetUUID, LLColor4::cyan));
		registrar.add("Minimap.Mark.Single.Yellow", boost::bind(&LLNetMap::markAgent, targetUUID, LLColor4::yellow));
		registrar.add("Minimap.Mark.Single.Custom", boost::bind(&LLNetMap::markAgent, targetUUID, customColor));
		registrar.add("Minimap.Mark.Single.Remove", boost::bind(&LLNetMap::markAgentRemove, targetUUID, menuName));
		registrar.add("Minimap.Zoom", boost::bind(&LLNetMap::handleZoom, this, _2));
	
		LLMenuGL* coreMenu =  LLUICtrlFactory::getInstance()->createFromFile<LLMenuGL>("exo_menu_mini_map_one.xml", gMenuHolder, LLViewerMenuHolderGL::child_registry_t::instance());
		
		coreMenu->setItemLabel("Target Name", menuName);
		coreMenu->setItemEnabled("Target Name", false);
		coreMenu->setItemFont("Target Name", LLFontGL::getFontSansSerifBold());

		coreMenu->setItemVisible("Add Friend", !show_as_friend);
		coreMenu->setItemEnabled("Stop Tracking", LLTracker::isTracking(0));
		coreMenu->setItemEnabled("Zoom In", is_not_null);
		
		const BOOL enabled = exoSystems::databaseBackendLocation.length() > 0;
		coreMenu->setItemVisible("Axis", enabled);
		coreMenu->setItemLabel("Axis", (exoSystems::databaseBackendLabel.length() > 0 ? exoSystems::databaseBackendLabel : "Axis") + "...");

		coreMenu->setItemColor("Offer Teleport", LLColor4::green);

		coreMenu->setItemEnabled("Request Teleport", show_as_friend);
		coreMenu->setItemColor("Request Teleport", LLColor4::cyan);

		coreMenu->setItemEnabled("Teleport To", is_not_null);
		coreMenu->setItemColor("Teleport To", LLColor4::yellow);

		LLMenuGL* subMenu = coreMenu->getChild<LLMenuItemBranchGL>("Moderation")->getBranch();
		if(subMenu)
		{
			bool can_freeze = LLAvatarActions::canLandFreezeOrEject(targetUUID);
			subMenu->setItemEnabled("Parcel Freeze", can_freeze);
			subMenu->setItemEnabled("Parcel Eject/Ban", can_freeze);

			bool can_estate = LLAvatarActions::canEstateKickOrTeleportHome(targetUUID);
			subMenu->setItemEnabled("Estate Kill", can_estate);
			subMenu->setItemEnabled("Estate Kick/Ban", can_estate);
		}

		subMenu = coreMenu->getChild<LLMenuItemBranchGL>("Mark")->getBranch();
		if(subMenu)
		{
			subMenu->setItemColor("Mark Friendly", friendlyColor);
			subMenu->setItemColor("Mark Hostile", hostileColor);

			subMenu->setItemColor("Mark Red", LLColor4::red);
			subMenu->setItemColor("Mark Green", LLColor4::green);
			subMenu->setItemColor("Mark Cyan", LLColor4::cyan);
			subMenu->setItemColor("Mark Yellow", LLColor4::yellow);

			subMenu->setItemColor("Mark Custom", customColor);
		}
		
		coreMenu->updateParent(LLMenuGL::sMenuContainer);
		coreMenu->setLabel(menuName);
		coreMenu->setCanTearOff(true);

		coreMenu->buildDrawLabels();

		LLMenuGL::showPopup(this, coreMenu, x, y);
	}
	else
	{
		LLUICtrl::CommitCallbackRegistry::ScopedRegistrar registrar;
		registrar.add("Minimap.Zoom", boost::bind(&LLNetMap::handleZoom, this, _2));
		registrar.add("Minimap.Tracker", boost::bind(&LLNetMap::handleStopTracking, this, _2));
		registrar.add("Minimap.Mark.All.Remove", boost::bind(&LLNetMap::markClear));
	
		LLMenuGL* coreMenu = LLUICtrlFactory::getInstance()->createFromFile<LLMenuGL>("menu_mini_map.xml", gMenuHolder, LLViewerMenuHolderGL::child_registry_t::instance());

		coreMenu->updateParent(LLMenuGL::sMenuContainer);
		coreMenu->setItemEnabled("Stop Tracking", LLTracker::isTracking(0));

		coreMenu->buildDrawLabels();

		LLMenuGL::showPopup(this, coreMenu, x, y);
	}
	// </exodus>

	return TRUE;
}

//gObjectList
//LLAvatarActions

void LLNetMap::openProfile(LLUUID target, std::string name)
{
	LLAvatarActions::showProfile(target);
}

void LLNetMap::addFriend(LLUUID target, std::string name)
{
	LLAvatarActions::requestFriendshipDialog(target, name);
}

void LLNetMap::instantMessage(LLUUID target, std::string name)
{
	LLAvatarActions::startIM(target);
}

void LLNetMap::zoomIn(LLUUID target, std::string name)
{
	LLAvatarActions::lookAtAvatar(target);
}

void LLNetMap::openAxis(LLUUID target, std::string name)
{
	LLAvatarActions::openAxis(target);
}

void LLNetMap::offerTeleport(LLUUID target, std::string name)
{
	LLAvatarActions::offerTeleport(target);
}

void LLNetMap::requestTeleport(LLUUID target, std::string name)
{
	if(!gAgent.mRequest.empty() && !gAgent.mRequestTimer.hasExpired())
	{
		LLSD args;
		args["MESSAGE"] = llformat("Teleport request replaced by request to \"%s\".", name.c_str());
		LLNotificationsUtil::add("SystemMessageTip", args);
	}

	std::string my_name;
	LLAgentUI::buildFullname(my_name);

	std::string request_string;
	LLUUID::generateNewID().toString(request_string);

	request_string = llformat("@exodus:teleport=%s", request_string.c_str());

	gAgent.mRequest = request_string;
	gAgent.mRequestTimer.start();
	gAgent.mRequestTimer.setTimerExpirySec(15.f);

	pack_instant_message(
		gMessageSystem,
		gAgent.getID(), FALSE,
		gAgent.getSessionID(),
		target, my_name,
		request_string,
		IM_ONLINE, IM_TYPING_STOP
	);

	gAgent.sendReliableMessage();
}

void LLNetMap::teleportTo(LLUUID target, LLVector3d position)
{
	if(!position.isNull())
	{
		if(position.mdV[VZ] == 0.f)
		{
			LLSD args;
			args["MESSAGE"] = "Unable to locate target, out of range.";
			LLNotificationsUtil::add("SystemMessageTip", args);
		}
		else gAgent.teleportViaLocation(position);
	}
}

void LLNetMap::moderationTools(LLUUID target, std::string type)
{
	if(type == "parcel freeze")
	{
		LLAvatarActions::landFreeze(target);
	}
	else if(type == "parcel eject/ban")
	{
		LLAvatarActions::landEject(target);
	}
	else if(type == "teleport home")
	{
		LLAvatarActions::estateTeleportHome(target);
	}
	else if(type == "estate kick/ban")
	{
		LLAvatarActions::estateKick(target);
	}
	else if(type == "report")
	{
		LLAvatarActions::report(target);
	}
}

void LLNetMap::markMassAgents(LLColor4 color)
{
	std::map<LLUUID, LLVector3d>::iterator current = mClosestAgentsAtLastClick.begin();
	std::map<LLUUID, LLVector3d>::iterator end = mClosestAgentsAtLastClick.end();
	for(; current != end; ++current) markAgent((*current).first, color);
}

void LLNetMap::markMassAgentsRemove()
{
	std::map<LLUUID, LLVector3d>::iterator current = mClosestAgentsAtLastClick.begin();
	std::map<LLUUID, LLVector3d>::iterator end = mClosestAgentsAtLastClick.end();
	for(; current != end; ++current) markAgentRemove((*current).first, "");
}

void LLNetMap::markClear()
{
	avatarColors.clear();
}

void LLNetMap::markAgent(LLUUID target,  LLColor4 color)
{
	std::map<LLUUID, LLColor4>::iterator found = avatarColors.find(target);
	if(found != avatarColors.end()) avatarColors.erase(found);

	avatarColors[target] = color;
}

void LLNetMap::markAgentRemove(LLUUID target, std::string name)
{
	std::map<LLUUID, LLColor4>::iterator found = avatarColors.find(target);
	if(found != avatarColors.end()) avatarColors.erase(found);
}
// </exodus>

BOOL LLNetMap::handleClick(S32 x, S32 y, MASK mask)
{
	// TODO: allow clicking an avatar on minimap to select avatar in the nearby avatar list
	// if(mClosestAgentToCursor.notNull())
	//     mNearbyList->selectUser(mClosestAgentToCursor);
	// Needs a registered observer i guess to accomplish this without using
	// globals to tell the mNearbyList in llpeoplepanel to select the user
	return TRUE;
}

BOOL LLNetMap::handleDoubleClick(S32 x, S32 y, MASK mask)
{
	LLVector3d pos_global = viewPosToGlobal(x, y);

	bool double_click_teleport = gSavedSettings.getBOOL("DoubleClickTeleport");
	bool double_click_show_world_map = gSavedSettings.getBOOL("DoubleClickShowWorldMap");

	if (double_click_teleport || double_click_show_world_map)
	{
		// If we're not tracking a beacon already, double-click will set one 
		if (!LLTracker::isTracking(NULL))
		{
			LLFloaterWorldMap* world_map = LLFloaterWorldMap::getInstance();
			if (world_map)
			{
				world_map->trackLocation(pos_global);
			}
		}
	}

	if (double_click_teleport)
	{
		// If DoubleClickTeleport is on, double clicking the minimap will teleport there
		gAgent.teleportViaLocationLookAt(pos_global);
	}
	else if (double_click_show_world_map)
	{
		LLFloaterReg::showInstance("world_map");
	}
	return TRUE;
}

// static
bool LLNetMap::outsideSlop( S32 x, S32 y, S32 start_x, S32 start_y, S32 slop )
{
	S32 dx = x - start_x;
	S32 dy = y - start_y;

	return (dx <= -slop || slop <= dx || dy <= -slop || slop <= dy);
}

BOOL LLNetMap::handleHover( S32 x, S32 y, MASK mask )
{
	if (hasMouseCapture())
	{
		if (mPanning || outsideSlop(x, y, mMouseDown.mX, mMouseDown.mY, MOUSE_DRAG_SLOP))
		{
			if (!mPanning)
			{
				// just started panning, so hide cursor
				mPanning = true;
				gViewerWindow->hideCursor();
			}

			LLVector2 delta(static_cast<F32>(gViewerWindow->getCurrentMouseDX()),
							static_cast<F32>(gViewerWindow->getCurrentMouseDY()));

			// Set pan to value at start of drag + offset
			mCurPan += delta;
			mTargetPan = mCurPan;

			gViewerWindow->moveCursorToCenter();
		}

		// Doesn't really matter, cursor should be hidden
		gViewerWindow->setCursor( UI_CURSOR_TOOLPAN );
	}
	else
	{
		if (mask & MASK_SHIFT)
		{
			// If shift is held, change the cursor to hint that the map can be dragged
			gViewerWindow->setCursor( UI_CURSOR_TOOLPAN );
		}
		else
		{
			gViewerWindow->setCursor( UI_CURSOR_CROSS );
		}
	}

	return TRUE;
}

void LLNetMap::handleZoom(const LLSD& userdata)
{
	std::string level = userdata.asString();
	
	F32 scale = 0.0f;
	if (level == std::string("default"))
	{
		LLControlVariable *pvar = gSavedSettings.getControl("ExodusMinimapScale"); // <exodus/>
		if(pvar)
		{
			pvar->resetToDefault();
			scale = gSavedSettings.getF32("ExodusMinimapScale"); // <exodus/>
		}
	}
	else if (level == std::string("close"))
		scale = LLNetMap::MAP_SCALE_MAX;
	else if (level == std::string("medium"))
		scale = LLNetMap::MAP_SCALE_MID;
	else if (level == std::string("far"))
		scale = LLNetMap::MAP_SCALE_MIN;
	if (scale != 0.0f)
	{
		setScale(scale);
	}
}

void LLNetMap::handleStopTracking (const LLSD& userdata)
{
	// <exodus>
	//if (coreMenu)
	{
		//coreMenu->setItemEnabled ("Stop Tracking", false);
		LLTracker::stopTracking ((void*)LLTracker::isTracking(NULL));
	}
	// </exodus>
}
