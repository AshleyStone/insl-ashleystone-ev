/** 
 * @file exofloateradvancedgraphics.h
 * @brief EXOFloaterAdvancedGraphics class definition
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXO_FLOATER_ADVANCED_GRAPHICS
#define EXO_FLOATER_ADVANCED_GRAPHICS

#include "llfloater.h"
#include "llassettype.h"
#include "llextendedstatus.h"

class LLComboBox;
class LLInventoryItem;
class LLVFS;

class exoFloaterAdvancedGraphics :
	public LLFloater
{
	friend class LLFloaterReg;

private:
	exoFloaterAdvancedGraphics(const LLSD& key);

	BOOL postBuild();
	void updateUI();
	
	bool updateNotes(const LLSD& newvalue);
	bool updateAmbientOcclusion(const LLSD& newvalue);
	bool updateToneMapping();
	void resetSetting(const LLSD& user_data);
	void resetSpecial(const LLSD& user_data);
	
	void addPreset();
	static void addPresetCallback(const LLSD& notification, const LLSD& response);

	void savePreset();
	static void savePresetCallback(const LLSD& notification, const LLSD& response);
	void savePresetFinal(std::string label);

	void removePreset();
	static void removePresetCallback(const LLSD& notification, const LLSD& response);

	void nextPreset();
	void previousPreset();

	void onSelectPreset(LLUICtrl* ctrl);
	void selectPreset(std::string requestName = "Default");
	
	void updateButtons(bool custom);

	LLSD generateData();
	
	void loadPresets(std::string dir, LLComboBox* combo, bool user = false);
	void loadPreset(LLSD data);

	void importFile();
	
public:
	void importNotecard(const LLInventoryItem* item);
	
	std::string loadNotecardName;

private:
	static void importNotecardCallback(const LLSD& notification, const LLSD& response);
	static void loadNotecard(LLVFS* vfs, const LLUUID& id, LLAssetType::EType type, void* userdata, S32 status, LLExtStat ext);

	void exportData();
	static void exportDataCallback(const LLSD& notification, const LLSD& response);
	void exportFile(std::string filename);
	void exportNotecard(std::string name);

public:
	LLUUID mInventoryFolder;

private:
	void onAdjustRSSE();
	void onAdjustRSSE2();

	void onAdjustGammaRed();
	void onAdjustGammaGreen();
	void onAdjustGammaBlue();

	void onAdjustGammaRedSpinner();
	void onAdjustGammaGreenSpinner();
	void onAdjustGammaBlueSpinner();

	void onAdjustExposureRed();
	void onAdjustExposureGreen();
	void onAdjustExposureBlue();

	void onAdjustExposureRedSpinner();
	void onAdjustExposureGreenSpinner();
	void onAdjustExposureBlueSpinner();

	void onAdjustOffsetRed();
	void onAdjustOffsetGreen();
	void onAdjustOffsetBlue();

	void onAdjustOffsetRedSpinner();
	void onAdjustOffsetGreenSpinner();
	void onAdjustOffsetBlueSpinner();
	
	void onAdjustVignetteX();
	void onAdjustVignetteY();
	void onAdjustVignetteZ();

	void onAdjustVignetteSpinnerX();
	void onAdjustVignetteSpinnerY();
	void onAdjustVignetteSpinnerZ();
	
	void onAdjustAdvToneShoulderStr();
	void onAdjustAdvToneLinearStr();
	void onAdjustAdvToneLinearAngle();
	void onAdjustAdvToneToeStr();
	void onAdjustAdvToneToeNumerator();
	void onAdjustAdvToneToeDenominator();
	void onAdjustAdvToneLinearWhitePoint();
	void onAdjustAdvToneExposureBias();
	
	void onAdjustAdvToneShoulderStrSpinner();
	void onAdjustAdvToneLinearStrSpinner();
	void onAdjustAdvToneLinearAngleSpinner();
	void onAdjustAdvToneToeStrSpinner();
	void onAdjustAdvToneToeNumeratorSpinner();
	void onAdjustAdvToneToeDenominatorSpinner();
	void onAdjustAdvToneLinearWhitePointSpinner();
	void onAdjustAdvToneExposureBiasSpinner();
	
	void onSetToneMappingMode();
};

#endif // EXO_FLOATER_ADVANCED_GRAPHICS
