/**
 * @file exostreamtitles.h
 * @brief Displays titles for the current stream as they change.
 *
 * $LicenseInfo:firstyear=2012&license=viewerlgpl$
 * Copyright (C) 2012 Katharine Berry
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llsingleton.h"

class exoStreamTitles : public LLSingleton<exoStreamTitles>
{
public:
	exoStreamTitles();
	static void initClass();
private:
	void onStreamMetadata(const std::string& artist, const std::string& title);
	bool onPump(const LLSD& data);
};
