/** 
 * @file exofloateradvancedradar.h
 * @brief EXOFloaterAdvancedRadar class definition
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXO_FLOATER_ADVANCED_RADAR
#define EXO_FLOATER_ADVANCED_RADAR

#include "llfloater.h"

class exoFloaterAdvancedRadar :
	public LLFloater
{
	friend class LLFloaterReg;

private:
	exoFloaterAdvancedRadar(const LLSD& key);

	BOOL postBuild();
};

#endif // EXO_FLOATER_ADVANCED_RADAR
